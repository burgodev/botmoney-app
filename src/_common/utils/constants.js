import {
  InteligenteFxIcon,
  MasterBotIcon,
  OptimusBotIcon,
  SmartBotIcon,
  FreeBot,
} from "../components/Icons";
import { theme } from "./theme";

export const BOTS = [
  {
    id: 0,
    name: "Free Bot",
    icon: <FreeBot width="77" height="77" />,
    color: theme.palette.green,
    recomendation: "conservadores",
    profitability: "2%",
    price: "Gratuito",
  },
  {
    id: 1,
    name: "Inteligente FX",
    icon: <InteligenteFxIcon width="77" height="77" />,
    color: theme.palette.blue,
    recomendation: "conservadores",
    profitability: "4%",
    price: "69,90",
  },
  {
    id: 2,
    name: "Master Bot",
    icon: <MasterBotIcon width="77" height="77" />,
    color: theme.palette.yellow,
    recomendation: "moderados",
    profitability: "6%",
    price: "69,90",
  },
  {
    id: 3,
    name: "Optimus Bot",
    icon: <OptimusBotIcon width="77" height="77" />,
    color: theme.palette.purple,
    recomendation: "moderados",
    profitability: "10%",
    price: "69,90",
  },
  {
    id: 4,
    name: "Smart Bot",
    icon: <SmartBotIcon width="77" height="77" />,
    color: theme.palette.red,
    recomendation: "moderados",
    profitability: "12%",
    price: "69,90",
  },
];
