import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import { useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";
import { fetchUser } from "../../../store/user";

import Menu from "../Menu";
import Sidebar from "../Sidebar";
import image from "../../../assets/bg1.jpg";
import backgroundImage from "../../../assets/backgroundImg.png";

PropTypes.propTypes = {
  children: PropTypes.object,
  classes: PropTypes.object,
};

const AppWrapper = ({ classes, children }) => {
  let location = useLocation();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchUser());
  }, [dispatch]);

  return (
    <div className={classes.wrapper}>
      <div className={classes.container}>
        {location.pathname !== "/" && <Menu />}
        <div
          className={classes.content}
          style={{ padding: location.pathname !== "/" ? "3vh 0 0" : null }}
        >
          {children}
        </div>
        {location.pathname !== "/" && <Sidebar />}
      </div>
    </div>
  );
};

export default withStyles((theme) => ({
  wrapper: {
    display: "flex",
    flexDirection: "column",
    // width: "100vw",
    height: "100vh",
    backgroundImage: `linear-gradient(to right bottom, rgb(255 255 255 / 90%), rgb(255 255 255 / 90%)),url(${image})`,
  },
  container: {
    display: "flex",
    width: "100%",
    height: "100%",
    backgroundImage: `linear-gradient(to right bottom, rgb(255 255 255 / 50%), rgb(255 255 255 / 50%)),url(${backgroundImage})`,
    backgroundSize: "cover",
  },

  content: {
    display: "flex",
    flexDirection: "column",
    width: "93.5vw",
    height: "100%",
    left: "6.5vw",

    overflow: "hidden",
    // padding: "5vh 3vw",
    // background: theme.palette.background,
    // backgroundSize: "contain",
    // backgroundImage: `linear-gradient(to right bottom, rgb(255 255 255 / 95%), rgb(255 255 255 / 99%)),url(${image})`,
    position: "fixed",
  },
}))(AppWrapper);
