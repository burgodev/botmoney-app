import React from "react";
import { createTheme, ThemeProvider } from "@material-ui/core/styles";
import { Button as MuiButton } from "@material-ui/core";
import PropTypes from "prop-types";

const theme = createTheme({
  overrides: {
    MuiButton: {
      text: {
        // width: 214,
        // height: 51,
        border: "1px solid #05C7F2",
        textTransform: "none",
        background: "transparent",
        // boxShadow: "4px 4px 4px rgba(0, 0, 0, 0.25)",
        borderRadius: 13,
        color: "#05C7F2",
        fontFamily: "Nunito",
        fontWeight: 600,
        fontSize: "1rem",
        "&:hover": {
          // backgroundColor: "white",
          // color: "#398CBF",
        },
      },
    },
  },
});

PropTypes.propTypes = {
  children: PropTypes.object,
};

const Contained = ({ children, ...props }) => {
  return (
    <ThemeProvider theme={theme}>
      <MuiButton {...props}>{children}</MuiButton>
    </ThemeProvider>
  );
};

export default Contained;
