import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { withStyles, Input, InputLabel, MenuItem } from "@material-ui/core";
import { Formik, Form, Field } from "formik";
import { TextField } from "formik-mui";
import { useSelector, useDispatch } from "react-redux";
import { fetchUser, saveUser } from "../../../../store/user";

import {
  Button,
  Flex,
  Typography,
  Loading,
} from "../../../../_common/components";
import TextMaskCpfCnpj from "../../../../_common/masks/TextMaskCpfCnpj";
import TextMaskNumber from "../../../../_common/masks/TextMaskNumber";

PropTypes.propTypes = {
  classes: PropTypes.object,
};

const ddd = ["047", "048", "049", "051"];

const MyData = ({ classes }) => {
  const { loading, data } = useSelector((state) => state.user);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchUser());
  }, [dispatch]);

  const handleSubmit = async (values) => {
    dispatch(saveUser(values));
  };

  if (loading) return <Loading isLoading className={classes.loading} />;

  return (
    <Formik initialValues={data} onSubmit={handleSubmit}>
      {(bag) => (
        <Form>
          <Typography className={classes.title}>Meus dados</Typography>
          {/* <Textfield
            label="teste"
            variant="formik"
            InputProps={{ color: "primary" }}
            // component={TextField}
            // input={<Input id="input-name-profile" />}
            name="name"
            type="text"
            className={classes.input}
            validate={(value) => (!value.length ? "Campo obrigatório*" : null)}
            size="small"
          ></Textfield> */}
          <Flex className={classes.flexWrapper}>
            <Flex flexDirection="column">
              <InputLabel
                className={classes.inputLabel}
                htmlFor="input-name-profile"
              >
                Nome completo
              </InputLabel>
              <Field
                component={TextField}
                variant="outlined"
                input={<Input id="input-name-profile" />}
                name="name"
                type="text"
                className={classes.input}
                validate={(value) =>
                  !value.length ? "Campo obrigatório*" : null
                }
                size="small"
              />
            </Flex>
            <Flex flexDirection="column">
              <InputLabel
                className={classes.inputLabel}
                htmlFor="input-birthay-profile"
              >
                Data Nascimento
              </InputLabel>
              <Field
                component={TextField}
                type="date"
                input={<Input id="input-birthay-profile" />}
                name="birthday"
                className={classes.input}
                validate={(value) =>
                  !value.length ? "Campo obrigatório*" : null
                }
                size="small"
              />
            </Flex>
            <Flex flexDirection="column">
              <InputLabel
                className={classes.inputLabel}
                htmlFor="input-cpf-profile"
              >
                CPF
              </InputLabel>
              <Field
                component={TextField}
                input={<Input id="input-cpf-profile" />}
                name="cpf"
                type="text"
                className={classes.input}
                size="small"
                validate={(value) =>
                  !value.length ? "Campo obrigatório*" : null
                }
                InputProps={{
                  inputComponent: TextMaskCpfCnpj,
                }}
              />
            </Flex>
            <Flex flexDirection="column">
              <InputLabel
                className={classes.inputLabel}
                htmlFor="input-telephone-profile"
              >
                Insira seu número de telefone (obrigatório)
              </InputLabel>
              <Flex>
                <Field
                  component={TextField}
                  input={<Input id="input-ddd-profile" />}
                  name="ddd"
                  size="small"
                  select
                  className={classes.select}
                >
                  {ddd.map((item) => (
                    <MenuItem id="menu-item-DDD" key={item} value={item}>
                      {item}
                    </MenuItem>
                  ))}
                </Field>

                <Field
                  component={TextField}
                  input={<Input id="input-telephone-profile" />}
                  name="telephone"
                  type="text"
                  className={classes.input}
                  size="small"
                  validate={(value) =>
                    !value.length ? "Campo obrigatório*" : null
                  }
                  InputProps={{
                    inputComponent: TextMaskNumber,
                    inputProps: { length: 20 },
                  }}
                />
              </Flex>
            </Flex>
            <Flex flexDirection="column" >
              <InputLabel
                className={classes.inputLabel}
                htmlFor="input-adress-profile"
              >
                Endereço
              </InputLabel>
              <Field
                component={TextField}
                input={<Input id="input-adress-profile" />}
                name="adress"
                type="text"
                validate={(value) =>
                  !value.length ? "Campo obrigatório*" : null
                }
                className={classes.input}
                size="small"
              />
            </Flex>
            <Flex flexDirection="column" className={classes.marginTop8}>
              <InputLabel
                className={classes.inputLabel}
                htmlFor="input-state-profile"
              >
                Estado
              </InputLabel>
              <Field
                component={TextField}
                input={<Input id="input-state-profile" />}
                name="state"
                type="text"
                validate={(value) =>
                  !value.length ? "Campo obrigatório*" : null
                }
                className={classes.input}
                size="small"
              />
            </Flex>
            <Flex flexDirection="column" className={classes.marginTop8}>
              <InputLabel
                className={classes.inputLabel}
                htmlFor="input-city-profile"
              >
                Cidade
              </InputLabel>
              <Field
                component={TextField}
                input={<Input id="input-city-profile" />}
                name="city"
                type="text"
                validate={(value) =>
                  !value.length ? "Campo obrigatório*" : null
                }
                className={classes.input}
                size="small"
              />
            </Flex>
          </Flex>
          <Flex className={classes.flexButton}>
            <Button type="submit" borderRadius={5}>
              Salvar
            </Button>
          </Flex>
        </Form>
      )}
    </Formik>
  );
};

// const INITIAL_VALUES = {
//   name: "",
//   birthday: "",
//   cpf: "",
//   telephone: "",
//   adress: "",
//   state: "",
//   city: "",
//   ddd: "047",
// };

export default withStyles((theme) => ({
  flexWrapper: {
    flexWrap: "wrap",
    marginTop: 24,
    width: "80%",
  },
  flexButton: {
    margin: "16px 16px 16px 0px",
  },
  input: {
    width: 224,
    marginTop: "4px !important",
    marginRight: "16px !important",
    background: theme.palette.common.white,
    // border: "1px solid #B2D4E4 !important",
    // boxSizing: "border-box",
    borderRadius: 5,
  },
  inputLabel: {
    fontSize: 16,
  },
  select: {
    // width: 63,
    // height: 39,
    background: "#FFFFFF",
    // border: "1px solid #B2D4E4",
    borderRadius: "5px",
    marginTop: "4px !important",
    marginRight: "2px !important",
  },
  marginTop8: {
    marginTop: 8,
  },
  title: {
    fontSize: 32,
    fontWeight: "bold",
    marginBottom: 16,
  },
  loading: {
    margin: "100px 0",
  },
}))(MyData);
