import React, { useState } from "react";
import { Formik, Form } from "formik";
import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import { withStyles, TextField } from "@material-ui/core";
import { useDispatch } from "react-redux";

import {
  Flex,
  Button,
  Loading,
  Typography,
} from "../../../../_common/components";

import { fetchUser } from "../../../../store/user";

PropTypes.propTypes = {
  classes: PropTypes.object,
  showRegister: PropTypes.func,
};

const initialValues = { email: "", password: "" };

function validMail(mail) {
  // eslint-disable-next-line no-useless-escape
  return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()\.,;\s@\"]+\.{0,1})+([^<>()\.,;:\s@\"]{2,}|[\d\.]+))$/.test(
    mail
  );
}

const validate = (values) => {
  const errors = {};

  if (!validMail(values.email)) {
    errors.email = "E-mail incorreto";
  }

  if (!values.password) {
    errors.password = "Senha incorreta";
  }

  console.log("errors", errors);

  return errors;
};

const LoginForm = ({ classes, showRegister }) => {
  const [loading, setLoading] = useState(false);
  let navigate = useNavigate();
  const dispatch = useDispatch();

  const handleSubmit = async (values) => {
    try {
      setLoading(true);
      dispatch(fetchUser());

      // api.post(`/user`, payload, {
      //   headers: { "Content-Type": "application/json" },
      // });
    } catch (error) {
      console.log(error.message);
    } finally {
      // dispatch(saveUser(values));

      setTimeout(() => {
        setLoading(false);
        navigate("/assinaturas/novas-assinaturas");
      }, 1500);
    }
  };

  const register = () => {
    showRegister();
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validate={validate}
      >
        {(bag) => (
          <Form>
            <Typography className={classes.typography}>Login</Typography>
            <Flex flexDirection="column" style={{ pading: 12, marginTop: 60 }}>
              <TextField
                variant="outlined"
                // fullWidth
                id="email"
                name="email"
                label="Número de telefone ou e-mail"
                // type="email"
                InputLabelProps={{ shrink: true }}
                size="small"
                value={bag.values.email}
                onChange={bag.handleChange}
                error={bag.touched.email && Boolean(bag.errors.email)}
                helperText={bag.touched.email && bag.errors.email}
              />
              <TextField
                variant="outlined"
                style={{ marginTop: 22 }}
                // fullWidth
                id="password"
                type="password"
                name="password"
                label="Senha"
                InputLabelProps={{ shrink: true }}
                size="small"
                value={bag.values.password}
                onChange={bag.handleChange}
                error={bag.touched.password && Boolean(bag.errors.password)}
                helperText={bag.touched.password && bag.errors.password}
              />

              <Button
                variant="text"
                style={{
                  fontSize: 15,
                  textTransform: "none",
                  paddingLeft: 0,
                  width: "fit-content",
                }}
              >
                Esqueceu sua senha?
              </Button>
            </Flex>
            <Flex flexDirection="column" style={{ marginTop: 60 }}>
              <Button
                variant="contained"
                type="submit"
                style={{
                  width: "100%",
                  fontSize: 18,
                  fontWeight: 600,
                  height: 40,
                }}
                disabled={loading}
              >
                {loading ? (
                  <Loading isLoading size={24} color="white" />
                ) : (
                  "Entrar"
                )}
              </Button>

              <Button
                variant="outlined"
                style={{
                  width: "100%",
                  marginTop: 16,
                  fontSize: 18,
                  fontWeight: 600,
                  height: 40,
                }}
                onClick={() => register()}
                disabled={loading}
              >
                Cadastrar
              </Button>
            </Flex>
          </Form>
        )}
      </Formik>

      <Flex center style={{ marginTop: 18 }}>
        <Typography textAlign="center" fontSize={15} width={290}>
          Dificuldades para acessar a plataforma? Entre em contato{" "}
          <Typography className={classes.blue}>suporte@botmoney.com</Typography>
        </Typography>
      </Flex>
    </>
  );
};

export default withStyles((theme) => ({
  container: {
    // opacity: 0,
    // paddingLeft: 0,
    // paddingRight: "7.5vw",
    // animation: `$translate .5s ease-out forwards`,

    // width: 1092,
    // height: 750,
    margin: "auto",
  },

  "@keyframes translate": {
    to: {
      // opacity: 1,
      // paddingLeft: "4vw",
    },
  },

  card: {
    height: 552,
    width: 450,
    zIndex: 1,
    borderRadius: 20,
    position: "fixed",
    top: "18.5%",
    right: "29%",
    padding: "28px 32px",
  },

  img: {
    transform: "translateY(-41px)",
    zIndex: 0,
    position: "fixed",
    top: "20%",
    left: "29%",
  },

  typography: {
    color: theme.palette.blue,
    fontSize: 32,
    fontWeight: "700",
  },
  blue: {
    color: theme.palette.blue,
  },
}))(LoginForm);
