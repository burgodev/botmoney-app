import React, { useState } from "react";
import { Formik, Form } from "formik";
import PropTypes from "prop-types";
import { withStyles, TextField, MenuItem } from "@material-ui/core";
import {
  Flex,
  Button,
  Loading,
  Typography,
} from "../../../../_common/components";

PropTypes.propTypes = {
  classes: PropTypes.object,
  showLogin: PropTypes.func,
};

const initialValues = { email: "", password: "", residency: "" };

function validMail(mail) {
  // eslint-disable-next-line no-useless-escape
  return /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()\.,;\s@\"]+\.{0,1})+([^<>()\.,;:\s@\"]{2,}|[\d\.]+))$/.test(
    mail
  );
}

const validate = (values) => {
  const errors = {};

  if (!validMail(values.email)) {
    errors.email = "E-mail inválido";
  }

  if (!values.email) {
    errors.email = "Campo obrigatório *";
  }

  if (!values.password) {
    errors.password = "Campo obrigatório *";
  }

  if (!values.residency) {
    errors.residency = "Campo obrigatório *";
  }

  console.log("errors", errors);

  return errors;
};

const RegisterForm = ({ classes, showLogin }) => {
  const [loading, setLoading] = useState(false);
  const [acceptPolicy, setAcceptPolicy] = useState(false);

  const handleSubmit = async (values) => {
    try {
      setLoading(true);

      // api.post(`/user`, payload, {
      //   headers: { "Content-Type": "application/json" },
      // });
    } catch (error) {
      console.log(error.message);
    } finally {
      // dispatch(saveUser(values));

      setTimeout(() => {
        setLoading(false);
        showLogin();
      }, 1500);
    }
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        onSubmit={handleSubmit}
        validate={validate}
      >
        {(bag) => (
          <Form>
            <Typography className={classes.typography}>Cadastro</Typography>
            <Flex flexDirection="column" style={{ pading: 12, marginTop: 32 }}>
              <TextField
                variant="outlined"
                // fullWidth
                id="email"
                name="email"
                label="Número de telefone ou e-mail"
                // type="email"
                InputLabelProps={{ shrink: true }}
                size="small"
                value={bag.values.email}
                onChange={bag.handleChange}
                error={bag.touched.email && Boolean(bag.errors.email)}
                helperText={bag.touched.email && bag.errors.email}
              />
              <TextField
                variant="outlined"
                style={{
                  marginTop:
                    bag.errors.email &&
                    bag.touched.email &&
                    bag.errors.password &&
                    bag.touched.password
                      ? 12
                      : 22,
                }}
                // fullWidth
                id="password"
                type="password"
                name="password"
                label="Senha"
                InputLabelProps={{ shrink: true }}
                size="small"
                value={bag.values.password}
                onChange={bag.handleChange}
                error={bag.touched.password && Boolean(bag.errors.password)}
                helperText={bag.touched.password && bag.errors.password}
              />

              <TextField
                variant="outlined"
                style={{
                  marginTop:
                    bag.errors.email &&
                    bag.touched.email &&
                    bag.errors.password &&
                    bag.touched.password &&
                    bag.errors.residency &&
                    bag.touched.residency
                      ? 12
                      : 22,
                }}
                // fullWidth
                select
                id="residency"
                type="residency"
                name="residency"
                label="Região/Nacionalidade"
                InputLabelProps={{ shrink: true }}
                size="small"
                value={bag.values.residency}
                onChange={bag.handleChange}
                error={bag.touched.residency && Boolean(bag.errors.residency)}
                helperText={bag.touched.residency && bag.errors.residency}
              >
                <MenuItem value={"Brasil"}>Brasil</MenuItem>
              </TextField>

              <Typography fontSize={12} style={{ marginTop: 4 }}>
                Certifique-se de que este é seu país de residência permanente
              </Typography>
            </Flex>
            <Flex style={{ marginTop: 16 }}>
              <input
                type="checkbox"
                id="scales"
                name="scales"
                checked={acceptPolicy}
                onChange={(e) => setAcceptPolicy(e.target.value)}
              />
              <Typography for="scales" fontSize={15} style={{ marginLeft: 4 }}>
                Eu concordo com os Termos e Condições e com a Política de
                Privacidade
              </Typography>
            </Flex>
            <Flex
              flexDirection="column"
              style={{
                marginTop:
                  (bag.errors.email &&
                    bag.touched.email &&
                    bag.errors.password &&
                    bag.touched.password) ||
                  (bag.errors.residency && bag.touched.residency)
                    ? 16
                    : 60,
              }}
            >
              <Button
                variant="contained"
                type="submit"
                style={{
                  width: "100%",
                  fontSize: 18,
                  fontWeight: 600,
                  height: 40,
                }}
                disabled={loading}
              >
                {loading ? (
                  <Loading isLoading size={24} color="white" />
                ) : (
                  "Cadastrar"
                )}
              </Button>
            </Flex>
          </Form>
        )}
      </Formik>

      <Flex center style={{ marginTop: 18 }}>
        <Typography textAlign="center" fontWeight="bold" fontSize={16}>
          Já possui uma conta?
        </Typography>
        <Button
          variant="text"
          style={{
            fontSize: 16,
            fontWeight: 600,
          }}
          width="none"
          disabled={loading}
          onClick={() => showLogin()}
        >
          Entrar agora
        </Button>
      </Flex>
    </>
  );
};

export default withStyles((theme) => ({
  container: {
    // opacity: 0,
    // paddingLeft: 0,
    // paddingRight: "7.5vw",
    // animation: `$translate .5s ease-out forwards`,

    // width: 1092,
    // height: 750,
    margin: "auto",
  },

  "@keyframes translate": {
    to: {
      // opacity: 1,
      // paddingLeft: "4vw",
    },
  },

  card: {
    height: 552,
    width: 450,
    zIndex: 1,
    borderRadius: 20,
    position: "fixed",
    top: "18.5%",
    right: "29%",
    padding: "28px 32px",
  },

  img: {
    transform: "translateY(-41px)",
    zIndex: 0,
    position: "fixed",
    top: "20%",
    left: "29%",
  },

  typography: {
    color: theme.palette.blue,
    fontSize: 32,
    fontWeight: "700",
  },
  blue: {
    color: theme.palette.blue,
  },
}))(RegisterForm);
