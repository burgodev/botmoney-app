import React, { useState } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";

import { Container, Card } from "../../_common/components";
import LoginImg from "../../assets/LoginImg.png";
import LoginForm from "./components/LoginForm";
import RegisterForm from "./components/RegisterForm";

PropTypes.propTypes = {
  classes: PropTypes.object,
};

const Login = ({ classes }) => {
  const [register, setRegister] = useState(false);

  const showRegister = () => {
    setRegister(true);
  };

  const showLogin = () => {
    setRegister(false);
  };
  return (
    <Container className={classes.container}>
      <img
        alt="login background"
        src={LoginImg}
        height={620}
        width={550}
        className={classes.img}
      />

      <Card className={classes.card}>
        {register ? (
          <RegisterForm showLogin={showLogin} />
        ) : (
          <LoginForm showRegister={showRegister} />
        )}
      </Card>
    </Container>
  );
};

export default withStyles((theme) => ({
  container: {
    // opacity: 0,
    // paddingLeft: 0,
    // paddingRight: "7.5vw",
    // animation: `$translate .5s ease-out forwards`,

    // width: 1092,
    // height: 750,
    margin: "auto",
  },

  "@keyframes translate": {
    to: {
      // opacity: 1,
      // paddingLeft: "4vw",
    },
  },

  card: {
    height: 552,
    width: 450,
    zIndex: 1,
    borderRadius: 20,
    position: "fixed",
    top: "18.5%",
    right: "29%",
    padding: "28px 40px",
  },

  img: {
    transform: "translateY(-41px)",
    zIndex: 0,
    position: "fixed",
    top: "20%",
    left: "29%",
  },

  typography: {
    color: theme.palette.blue,
    fontSize: 32,
    fontWeight: "700",
  },
  blue: {
    color: theme.palette.blue,
  },
}))(Login);
