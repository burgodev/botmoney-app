import React, { useState } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import { Link } from "react-router-dom";

import {
  Flex,
  Typography,
  Container,
  Divider,
  Button,
} from "../../../_common/components";

// import CardAvailableSignature from "./components/CardAvailableSignature";
import CardMySignature from "../components/CardMySignature";

PropTypes.propTypes = {
  classes: PropTypes.object,
};

const MySignatures = ({ classes }) => {
  const [hasSignature /*, setHasSignature*/] = useState(true);

  return (
    <Container className={classes.container}>
      <Flex center flexDirection="column" width="100%">
        <Typography className={classes.pageTitle}>Assinaturas</Typography>
        <Divider margin="16px 0 0" />
        {/* {!hasSignature && (
          <>
            <Typography className={classes.subtitle1}>
              Minhas assinaturas
            </Typography>
            <Typography className={classes.subtitle2}>
              Você não possui nenhuma assinatura
            </Typography>
          </>
        )} */}
      </Flex>
      {/* <Flex className={classes.flexMargin}>
        <Flex justifyContent="flex-end" width="50%">
          {!hasSignature && (
            <Card className={classes.cardTop}>
              <Flex>
                <MoneyBagIcon />
                <Typography element="p" className={classes.typography1}>
                  Automatizando suas operações, nós cuidamos de obsolutamente
                  tudo pra você, de acor com o seu peril de investidor sendo
                  conservador, moderado e arrojado. Basta você realizar o
                  cadastro em nossa corretora parceira. Depositar o valor do
                  capital operacional, selecionar um robô e dar start em suas
                  operações 100% automatizadas.
                </Typography>
              </Flex>
            </Card>
          )}
        </Flex>
      </Flex> */}

      {hasSignature && <CardMySignature />}
      {/* <CardAvailableSignature /> */}

      {/* <Flex justifyContent="flex-end">
        {!hasSignature && (
          <Card className={classes.cardBottom}>
            <Flex alignItems="center">
              <BonfireIcon />
              <Typography element="p" className={classes.typography2}>
                Implementamos uma nova estratégia operacional em parceria com a
                corretora Select FX, apresentando a você o SmartBot, com esse
                robô escrevemos uma nova história no ramo de automação de
                negociações dentro do mercado de forex.
              </Typography>
            </Flex>
          </Card>
        )}
      </Flex> */}
      <Flex center style={{ marginTop: 8 }}>
        <Link className={classes.link} to={"/assinaturas/novas-assinaturas"}>
          <Button className={classes.button}>Nova Assinatura</Button>
        </Link>
      </Flex>
    </Container>
  );
};

export default withStyles((theme) => ({
  container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    opacity: 0,
    paddingBottom: "5vh",
    paddingLeft: 0,
    paddingRight: "4vw",
    // paddingTop: "1.5vh",
    animation: `$translate .5s ease-out forwards`,
  },
  "@keyframes translate": {
    to: {
      opacity: 1,
      paddingLeft: "4vw",
    },
  },
  pageTitle: {
    fontWeight: "bold",
    fontSize: 28,
  },
  button: {
    background: theme.palette.green,
    width: 440,
    boxShadow: "1px 1px 20px #69BF41",
    borderRadius: 15,
    "&:hover": {
      background: theme.palette.green,
    },
  },
  link: {
    textDecoration: "none",
  },
  // subtitle1: {
  //   fontWeight: "bold",
  //   fontSize: 24,
  //   marginTop: 55,
  //   marginBottom: 7,
  // },
  // subtitle2: {
  //   fontWeight: "bold",
  //   fontSize: 20,
  //   color: "#CBCBCB",
  // },
  // cardBottom: {
  //   borderRadius: "30px 0px",
  //   padding: "20px 15px 15px 20px",
  //   marginTop: 4,
  // },
  // cardTop: {
  //   borderRadius: "30px 0px",
  //   padding: "20px 15px 15px 20px",
  // },
  // typography1: {
  //   width: 394,
  //   marginTop: 0,
  //   marginLeft: 15,
  //   fontSize: 16,
  // },
  // typography2: {
  //   fontWeight: "bold",
  //   fontSize: 12,
  //   width: 394,
  //   marginTop: 0,
  //   marginLeft: 15,
  // },
  // flexMargin: {
  //   marginBottom: 8,
  // },
}))(MySignatures);
