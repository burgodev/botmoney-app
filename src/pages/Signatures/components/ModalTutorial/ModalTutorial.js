import React from "react";
import PropTypes from "prop-types";
// import { TextField as FormikTextField } from "formik-mui";
import {
  DialogTitle,
  DialogContent,
  DialogActions,
  withStyles,
} from "@material-ui/core";

import CloseIcon from "@material-ui/icons/Close";
import bots from "../../../../assets/bots.png";
import tokenImage from "../../../../assets/token.png";
import payment from "../../../../assets/payment.png";
import review from "../../../../assets/review.png";
import broker from "../../../../assets/broker.png";
import { Flex, Typography, Card, Button } from "../../../../_common/components";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import { useSelector } from "react-redux";

PropTypes.propTypes = {
  classes: PropTypes.object,
  onClose: PropTypes.func.isRequired,
  operations: PropTypes.array,
};

const ModalTutorial = ({ classes, onClose, operations }) => {
  const [step, setStep] = React.useState(0);
  const user = useSelector((state) => state.user.data);

  console.log("user", user);

  // const handleSubmit = async (values) => {
  //   try {
  //     api.post(`/user`, payload, {
  //       headers: { "Content-Type": "application/json" },
  //     });
  //   } catch (error) {}
  //   dispatch(saveUser(values));
  // };

  return (
    <Card className={classes.card}>
      <DialogTitle disableTypography className={classes.title}>
        <Flex justifyContent="flex-end" width="100%">
          <Button
            variant="icon"
            onClick={() => onClose()}
            className={classes.iconButton}
          >
            <CloseIcon />
          </Button>
        </Flex>
        <Typography fontWeight="bold" color="#FFF" fontSize={30}>
          Botmoney
        </Typography>
      </DialogTitle>
      <DialogContent className={classes.content}>
        {step === 0 && (
          <>
            <Typography fontWeight="bold" color="#FFF" fontSize={40}>
              Seja bem-vindo(a)
            </Typography>
            <Typography
              fontWeight="bold"
              color="#FFF"
              fontSize={32}
              style={{
                border: "2px solid #CBCBCB",
                padding: "8px 160px",

                borderRadius: 11,
              }}
            >
              {user ? user.name.split(" ")[0] : ""}
            </Typography>

            <Typography color="#FFF" fontSize={20} textAlign="center">
              A seguir algumas instruções para seu primeiro login em nossa
              plataforma.
            </Typography>
          </>
        )}

        {step === 1 && (
          <>
            <Typography
              fontWeight="bold"
              color="#FFF"
              fontSize={30}
              textAlign="center"
            >
              1. Integre sua corretora com a plataforma
            </Typography>

            <img
              src={tokenImage}
              style={{ borderRadius: 24 }}
              // width={320}
              alt="Imagem de exemplo para o step de integração com sua corretora"
            />

            <div></div>
            {/* 
            <Typography color="#FFF" fontSize={20} textAlign="center">
              Não possui conta em nenhuma corretora?
            </Typography>

            <Button className={classes.buttonSelect}>
              <SelectLogo />
            </Button> */}
          </>
        )}
        {step === 2 && (
          <>
            <Typography
              fontWeight="bold"
              color="#FFF"
              fontSize={30}
              textAlign="center"
            >
              2. Escolha um robô para simular ou assinar na plataforma.
            </Typography>

            <img
              src={bots}
              width={200}
              height={260}
              style={{ borderRadius: 24 }}
              alt="Imagem de exemplo para o step de escolha seu bot"
            />

            <Typography
              element="p"
              textAlign="center"
              color="white"
              fontSize={14}
              width="85%"
            >
              Automatizando suas operações, nós cuidamos de obsolutamente tudo
              pra você, de acordo com o seu peril de investidor sendo{" "}
              <span className={classes.blue}> conservador</span>,
              <span className={classes.yellow}> moderado </span> e
              <span className={classes.red}> arrojado</span>.
            </Typography>
          </>
        )}
        {step === 3 && (
          <>
            <Typography
              fontWeight="bold"
              color="#FFF"
              fontSize={30}
              textAlign="center"
            >
              3. Escolha sua conta na corretora
            </Typography>
            <img
              src={broker}
              width={500}
              style={{ borderRadius: 24 }}
              alt="Imagem de exemplo para o step de escolha seu bot"
            />

            <Typography
              color="#FFF"
              fontSize={20}
              textAlign="center"
            ></Typography>
          </>
        )}
        {step === 4 && (
          <>
            <Typography
              fontWeight="bold"
              color="#FFF"
              fontSize={30}
              textAlign="center"
            >
              4. Realize o pagamento ou faça uma simulação gratuitamente
            </Typography>

            <img
              src={payment}
              style={{ borderRadius: 24 }}
              // width={320}
              alt="Imagem de exemplo para o step de integração com sua corretora"
            />

            <Typography
              color="#FFF"
              fontSize={20}
              textAlign="center"
            ></Typography>
          </>
        )}
        {step === 5 && (
          <>
            <Typography
              fontWeight="bold"
              color="#FFF"
              fontSize={30}
              textAlign="center"
            >
              5. Revise e confirme os seus dados
            </Typography>
            <img
              src={review}
              style={{ borderRadius: 24 }}
              width={420}
              alt="Imagem de exemplo para o step de integração com sua corretora"
            />
            <Typography
              color="#FFF"
              fontSize={20}
              textAlign="center"
            ></Typography>
          </>
        )}
        {step === 6 && (
          <>
            <Typography fontWeight="bold" color="#FFF" fontSize={40}>
              Vamos começar?
            </Typography>
            <Typography color="#FFF" fontSize={20} textAlign="center">
              Você já sabe tudo que precisa para usar nossa plataforma!
            </Typography>
            <Typography
              color="#FFF"
              fontSize={20}
              textAlign="center"
            ></Typography>
            <Flex center flexDirection="column" width="100%">
              <Button onClick={onClose} className={classes.button}>
                Vamos começar
              </Button>
            </Flex>
          </>
        )}
      </DialogContent>
      <DialogActions>
        <Flex
          width="100%"
          alignItems="center"
          justifyContent="space-between"
          style={{ padding: "0 24px" }}
          flexDirection="column"
        >
          {step === 0 && (
            <Button
              onClick={() => setStep(step + 1)}
              className={classes.button}
            >
              Vamos começar
            </Button>
          )}

          {step !== 0 && step !== 6 && (
            <Button
              variant="icon"
              className={classes.iconButton}
              onClick={() => (step === 6 ? onClose() : setStep(step + 1))}
            >
              <ArrowForwardIcon fontSize="large" />
            </Button>
          )}
        </Flex>
      </DialogActions>
    </Card>
  );
};

export default withStyles((theme) => ({
  title: {
    padding: 0,
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    flexDirection: "column",
    // flexDirection: "column",
    // alignItems: "start",
  },

  button: {
    textTransform: "capitalize",
    marginBottom: 18,
    padding: 0,
    color: "#727272",

    // height: 32,

    width: "75%",
    height: 52,

    background: "#F6F6F6",
    boxShadow: "4px 4px 10px rgba(0, 0, 0, 0.25)",
    borderRadius: 11,

    "&:hover": {
      background: "white",
    },
  },

  card: {
    backgroundImage: theme.palette.gradients.image,
    boxShadow: "10px 10px 50px rgba(0, 0, 0, 0.5)",
    padding: "24px 31px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",

    // backgroundImage: `linear-gradient(to right bottom, rgb(255 255 255 / 95%), rgb(255 255 255 / 97.5%)),url(${image})`,
    // background: "transparent",
    height: 680,

    // background: "linear-gradient(151.49deg, #9746FF -0.14%, #05C7F2 128.01%)",
  },
  iconButton: {
    color: theme.palette.white,
    borderRadius: "50%",
  },
  content: {
    // height: 300,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
    padding: "50px 24px",
    overflow: "hidden",
  },
  buttonSelect: {
    background: "white",
    border: "2px solid #CBCBCB",
    width: 450,
    borderRadius: 5,
    height: 44,
  },
  blue: { color: theme.palette.blue },
  yellow: { color: theme.palette.yellow },
  red: { color: theme.palette.red },
}))(ModalTutorial);
