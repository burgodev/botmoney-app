import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { withStyles, Grid } from "@material-ui/core";

import {
  Flex,
  Typography,
  Card,
  Button,
  Divider,
  Container,
  Loading,
} from "../../../../_common/components";
import api from "../../../../services/api";
import { CancelIcon, ConfirmIcon } from "../../../../_common/components/Icons";
// import { theme } from "../../../../_common/utils/theme";
import { BOTS } from "../../../../_common/utils/constants";

PropTypes.propTypes = {
  classes: PropTypes.object,
};

const CardMySignature = ({ classes }) => {
  const [data, setData] = useState();
  const [, /* openModal */ setOpenModal] = useState(false);

  useEffect(() => {
    getDashboard();
  }, []);

  const getDashboard = async () => {
    try {
      const resp = await api.get("/subscriptions");
      setTimeout(() => setData(resp.data), 1000);
    } catch (e) {
      console.log("error dashboard", e);
    }
  };

  return (
    <Container>
      <Flex className={classes.flexTitle}>
        <Typography className={classes.title}>Minhas assinaturas</Typography>
      </Flex>
      <Grid container>
        <Grid item xs={2} />
        <Grid item xs={2} className={classes.textCenter}>
          <Typography>Datas da contratação</Typography>
        </Grid>
        <Grid item xs={2} className={classes.textCenter}>
          <Typography>Ordens realizadas</Typography>
        </Grid>
        <Grid item xs={2} className={classes.textCenter}>
          <Typography>Dias ativos do rôbo</Typography>
        </Grid>
        <Grid item xs={2} className={classes.textCenter}>
          <Typography>Valor pago</Typography>
        </Grid>
        <Grid item xs={2} className={classes.textCenter}>
          <Typography>Lucro</Typography>
        </Grid>
      </Grid>
      {typeof data === "undefined" ? (
        <Loading size={60} className={classes.loading} isLoading />
      ) : (
        data?.map((item) => {
          return (
            <Card key={item.id} className={classes.card}>
              <Grid container>
                <Grid
                  item
                  xs={1}
                  className={`${classes.grid} ${classes.gridIcon}`}
                >
                  {BOTS.find(({ id }) => item.id === id).icon}
                </Grid>

                <Grid item xs={1}>
                  <Flex flexDirection="column">
                    <Typography fontSize={18} fontWeight={600} italic>
                      {item.name}
                    </Typography>
                    <Typography
                      fontSize={16}
                      fontWeight={600}
                      style={{
                        color: BOTS.find(({ id }) => item.id === id).color,
                      }}
                    >
                      Ativo
                    </Typography>
                  </Flex>
                </Grid>

                <Grid item xs={2} className={classes.grid}>
                  <Typography>{item.hiringDates}</Typography>
                </Grid>
                <Grid item xs={2} className={classes.grid}>
                  <Typography>{item.ordersPlaced}</Typography>
                </Grid>
                <Grid item xs={2} className={classes.grid}>
                  <Typography>{item.ordersCompleted}</Typography>
                </Grid>
                <Grid item xs={2} className={classes.grid}>
                  <Typography>{item.amountPaid}</Typography>
                </Grid>
                <Grid item xs={2} className={classes.grid}>
                  <Typography>{item.profit}</Typography>
                </Grid>

                <Divider margin={"4px 0 8px"} />

                <Grid item xs={1}>
                  <Card className={classes.card2}>
                    <Typography fontSize={16} fontWeight={600} color="white">
                      Simulação
                    </Typography>
                  </Card>
                </Grid>

                <Grid item xs={1}>
                  <Typography fontSize={16} fontWeight={600}>
                    Conta: ID {item.id}
                  </Typography>
                </Grid>
                <Grid item xs={2} className={classes.grid}>
                  <Typography
                    className={classes.typography}
                    style={{
                      color: BOTS.find(({ id }) => item.id === id).color,
                    }}
                  >
                    Montante:
                  </Typography>
                  <Typography fontWeight={600}>{item.amount}</Typography>
                </Grid>

                <Grid item xs={2} className={classes.grid}>
                  <Typography
                    className={classes.typography}
                    style={{
                      color: BOTS.find(({ id }) => item.id === id).color,
                    }}
                  >
                    Lucros:
                  </Typography>
                  <Typography fontWeight={600}>{item.profit}</Typography>
                </Grid>

                <Grid item xs={2} className={classes.grid}>
                  <Typography
                    className={classes.typography}
                    style={{
                      color: BOTS.find(({ id }) => item.id === id).color,
                    }}
                  >
                    Saldo total:
                  </Typography>

                  <Typography fontWeight={600}>{item.totalBalance}</Typography>
                </Grid>

                <Grid item xs={1} />

                <Grid item xs={3} className={classes.grid}>
                  <Button
                    variant="text"
                    className={classes.button}
                    onClick={() => setOpenModal(true)}
                  >
                    <ConfirmIcon className={classes.icon} />
                    Renovar assinatura
                  </Button>
                  <Button variant="text" className={classes.buttonCancel}>
                    <CancelIcon className={classes.icon} />
                    Cancelar assinatura
                  </Button>
                </Grid>
              </Grid>
            </Card>
          );
        })
      )}
    </Container>
  );
};

export default withStyles((theme) => ({
  card: {
    borderRadius: 13,
    marginBottom: 16,
    padding: "12px 0px",
  },
  title: {
    width: 245,
    fontWeight: "bold",
    fontSize: 24,
  },
  typography: {
    marginRight: 16,
    fontWeight: 600,
  },
  button: {
    color: theme.palette.green,
    textTransform: "none",
    fontSize: 12,
    fontWeight: 600,
    lineHeight: "12px",
  },
  buttonCancel: {
    color: theme.palette.red,
    textTransform: "none",
    fontSize: 12,
    fontWeight: 600,
    lineHeight: "12px",
    marginLeft: 16,
  },
  icon: {
    marginRight: 4,
    height: 16,
    width: 16,
  },
  card2: {
    width: 105,
    height: 28,
    background: "#68A64B",
    borderRadius: "0px 10px 10px 0px",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  grid: {
    textAlign: "center",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  textCenter: {
    textAlign: "center",
    marginBottom: 4,
  },
  flexTitle: {
    alignItems: "center",
    margin: "16px 0 24px",
  },
  loading: {
    height: "60%",
  },
  gridIcon: {
    paddingRight: 16,
  },
}))(CardMySignature);
