import React from "react";
import { useFormikContext } from "formik";
import PropTypes from "prop-types";
import { withStyles, TextField } from "@material-ui/core";

import { SelectLogo } from "../../../../_common/components/Icons";
import {
  Flex,
  Typography,
  Button,
  Card,
  Divider,
  Loading,
} from "../../../../_common/components";

PropTypes.propTypes = {
  classes: PropTypes.object,
  nextStep: PropTypes.func,
  loading: PropTypes.bool,
};

const IntegrationToken = ({ classes, nextStep, loading }) => {
  const { values, touched, errors, handleChange } = useFormikContext();

  // const integrateToken = () => {
  //   try {
  //     setLoading(true);
  //   } catch (e) {
  //   } finally {
  //     setTimeout(() => setLoading(false), 1000);
  //     setTimeout(() => nextStep(), 1000);
  //   }
  // };

  return (
    <Card borderRadius={27} className={classes.card}>
      <Flex className={classes.flex}>
        <Flex
          justifyContent="center"
          alignItems="center"
          flexDirection="column"
        >
          <Typography className={classes.pageTitle}>
            Faça a integração da sua corretora com a{" "}
            <Typography color="#05C7F2">Botmoney</Typography>
          </Typography>

          <Typography className={classes.subtitle}>
            Insira o código no campo abaixo
          </Typography>

          {/* <TextField
            variant="outlined"
            size="small"
            id="textfield-dashboard-link"
            className={classes.textfield}
            value={token}
            onChange={(e) => setToken(e.target.value)}
          /> */}

          <TextField
            variant="outlined"
            fullWidth
            id="input-integrationToken-signatures"
            type="token"
            name="token"
            label="Token"
            InputLabelProps={{ shrink: true }}
            className={classes.textfield}
            size="small"
            value={values.token}
            onChange={handleChange}
            error={touched.token && Boolean(errors.token)}
            helperText={touched.token && errors.token}
          />

          <Button
            // color="primary"
            style={{ marginTop: errors.token ? 20 : null }}
            // onClick={integrateToken}
            className={classes.button}
            // disabled={!values.token.length}
            type="submit"
          >
            {loading ? (
              <Loading isLoading size={24} color="white" />
            ) : (
              "Integrar"
            )}
          </Button>
        </Flex>

        <Divider margin="40px 0 20px" width={450} />
        <Typography className={classes.typography}>
          Não possui conta em nenhuma corretora?
        </Typography>
        <Button className={classes.buttonSelect}>
          <SelectLogo />
        </Button>
      </Flex>
    </Card>
  );
};

export default withStyles((theme) => ({
  button: {
    width: 450,
    height: 44,
    borderRadius: "6px",
    marginTop: 8,
  },
  buttonSelect: {
    background: "white",
    border: "2px solid #CBCBCB",
    width: 450,
    borderRadius: 5,
    height: 44,
  },
  flex: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    padding: "24px 32px",
  },
  textfield: {
    width: 450,
    height: 55,
  },
  card: {
    height: "100%",
    background: "transparent",
    width: "max-content",
    margin: "auto",

    opacity: 0,
    transform: "translateX(-50px)",
    animation: `$translate .5s ease-out forwards`,
  },

  "@keyframes translate": {
    to: {
      opacity: 1,
      transform: "translateX(0)",
    },
  },

  pageTitle: {
    fontSize: 28,
    marginTop: 8,
    marginBottom: 84,
  },
  subtitle: {
    fontSize: 15,
    marginTop: 12,
    marginBottom: 8,
  },
  typography: {
    fontSize: 15,
    marginBottom: 8,
  },
}))(IntegrationToken);
