import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core";
import { useFormikContext } from "formik";

import {
  Flex,
  Typography,
  Card,
  Button,
  TextField,
} from "../../../../_common/components";
import TextMaskNumber from "../../../../_common/masks/TextMaskNumber";
import TextMaskCpfCnpj from "../../../../_common/masks/TextMaskCpfCnpj";

PropTypes.propTypes = {
  classes: PropTypes.object,
  values: PropTypes.object,
  nextStep: PropTypes.func,
};

const Payment = ({ classes, nextStep }) => {
  const { values, errors, touched, handleChange } = useFormikContext();

  return (
    <Flex center className={classes.flex}>
      <Card className={classes.card}>
        <Flex center>
          <Typography className={classes.pageTitle}>
            Métodos de pagamento
          </Typography>
        </Flex>

        <TextField
          color={values.bot.color}
          variant="outlined"
          fullWidth
          id="cardNumber"
          InputProps={{
            inputComponent: TextMaskNumber,
            inputProps: { length: 16 },
          }}
          className={classes.marginTop}
          name="payment.cardNumber"
          size="small"
          s
          label="Número do Cartão"
          value={values.payment.cardNumber}
          onChange={handleChange}
          error={
            touched.payment?.cardNumber && Boolean(errors.payment?.cardNumber)
          }
          helperText={touched.payment?.cardNumber && errors.payment?.cardNumber}
          InputLabelProps={{ shrink: true }}
        />

        <Flex width="90%" className={classes.marginTop}>
          <TextField
            color={values.bot.color}
            variant="outlined"
            fullWidth
            style={{
              width: errors.payment?.cvv ? 400 : 240,
            }}
            id="dueDate"
            name="payment.dueDate"
            size="small"
            label="Data de vencimento"
            type="date"
            value={values.payment.dueDate}
            onChange={handleChange}
            error={touched.payment?.dueDate && Boolean(errors.payment?.dueDate)}
            helperText={touched.payment?.dueDate && errors.payment?.dueDate}
            InputLabelProps={{ shrink: true }}
          />
          <TextField
            color={values.bot.color}
            variant="outlined"
            style={{
              width: errors.payment?.cvv ? null : 60,
              marginLeft: 16,
            }}
            id="cvv"
            name="payment.cvv"
            InputProps={{
              inputComponent: TextMaskNumber,
              inputProps: { length: 3 },
            }}
            size="small"
            label="CVV"
            value={values.payment.cvv}
            onChange={handleChange}
            error={touched.payment?.cvv && Boolean(errors.payment?.cvv)}
            helperText={touched.payment?.cvv && errors.payment?.cvv}
            InputLabelProps={{ shrink: true }}
          />
        </Flex>

        <TextField
          color={values.bot.color}
          className={classes.marginTop}
          variant="outlined"
          fullWidth
          id="value"
          name="payment.name"
          size="small"
          label="Nome do titular do cartão"
          value={values.payment.name}
          onChange={handleChange}
          error={touched.payment?.name && Boolean(errors.payment?.name)}
          helperText={touched.payment?.name && errors.payment?.name}
          InputLabelProps={{ shrink: true }}
        />

        <TextField
          color={values.bot.color}
          className={classes.marginTop}
          variant="outlined"
          fullWidth
          id="value"
          name="payment.cpf"
          size="small"
          label="CPF"
          value={values.payment.cpf}
          onChange={handleChange}
          error={touched.payment?.cpf && Boolean(errors.payment?.cpf)}
          helperText={touched.payment?.cpf && errors.payment?.cpf}
          InputLabelProps={{ shrink: true }}
          InputProps={{
            inputComponent: TextMaskCpfCnpj,
          }}
        />

        <TextField
          color={values.bot.color}
          className={classes.marginTop}
          variant="outlined"
          fullWidth
          id="discount"
          label="Cupom de desconto"
          name="payment.discount"
          size="small"
          value={values.payment.discount}
          onChange={handleChange}
          error={touched.payment?.discount && Boolean(errors.payment?.discount)}
          helperText={touched.payment?.discount && errors.payment?.discount}
          InputLabelProps={{ shrink: true }}
        />

        <Typography element="p" textAlign="center" fontSize={14}>
          Automatizando suas operações, nós cuidamos de obsolutamente tudo pra
          você, de acordo com o seu peril de investidor sendo
          <strong> conservador, moderado e arrojado</strong>. Basta você
          realizar o cadastro em nossa corretora parceira. Depositar o valor do
          capital operacional, selecionar um robô e dar start em suas operações
          100% automatizadas.
        </Typography>

        <Button
          type="submit"
          className={classes.button}
          style={{
            boxShadow: `1px 1px 20px ${values.bot.color}`,
            background: values.bot.color,
          }}
        >
          Confirmar
        </Button>
      </Card>
    </Flex>
  );
};

export default withStyles((theme) => ({
  flex: {
    opacity: 0,
    transform: "translateX(-50px)",
    animation: `$translate .5s ease-out forwards`,
  },
  "@keyframes translate": {
    to: {
      opacity: 1,
      transform: "translateX(0)",
    },
  },

  pageTitle: {
    fontWeight: "bold",
    fontSize: 28,
    margin: "auto",
    marginBottom: 32,
  },
  card: {
    width: 480,
    background: "transparent",
  },
  marginTop: {
    marginTop: 16,
  },
  button: {
    width: "100%",
    height: 44,
    margin: "16px auto 0",
  },
}))(Payment);
