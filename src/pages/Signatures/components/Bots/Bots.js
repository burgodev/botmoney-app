import React from "react";
import PropTypes from "prop-types";
import { useFormikContext } from "formik";
import { withStyles } from "@material-ui/core";

import {
  Flex,
  Typography,
  Card,
  Divider,
  Button,
  Container,
} from "../../../../_common/components";
import { BOTS } from "../../../../_common/utils/constants";

PropTypes.propTypes = {
  classes: PropTypes.object,
  nextStep: PropTypes.func,
};

const Bots = ({ classes, nextStep }) => {
  const { setFieldValue } = useFormikContext();

  return (
    <Container className={classes.container}>
      <Flex center flexDirection="column">
        <Typography className={classes.pageTitle}>
          Escolha o plano de assinaturas ideal para você
        </Typography>
        <Flex center>
          <Typography element="p" width={590}>
            Automatizando suas operações, nós cuidamos de obsolutamente tudo pra
            você, de acordo com o seu perfil de investidor sendo
            <span className={classes.blue}> conservador</span>,
            <span className={classes.yellow}> moderado </span> e
            <span className={classes.red}> arrojado</span>.
          </Typography>
        </Flex>
      </Flex>
      <Flex center className={classes.flex}>
        {BOTS.map((bot) => (
          <div>
            <Card key={bot.id} className={classes.card}>
              <Flex
                flexDirection="column"
                center
                className={classes.translateY}
              >
                {bot.icon}
                <Typography className={classes.name}>{bot.name}</Typography>
                <Typography textAlign="center" width={200}>
                  Recomendado para investidor {bot.recomendation}
                </Typography>
                <Divider width="80%" margin="12px 0 18px 0" />
                <Typography textAlign="center">Retorno Aproximado</Typography>
                <Typography color={bot.color} margin="14px 0" fontSize={32}>
                  {bot.profitability}
                </Typography>
                <Typography fontWeight="bold" fontSize={24}>
                  {bot.id !== 0 && "R$ "}
                  {bot.price}
                </Typography>
              </Flex>
            </Card>
            <Flex center flexDirection="column">
              <Button
                background={bot.color}
                width={174}
                height={42}
                style={{ boxShadow: `1px 1px 20px ${bot.color}` }}
                onClick={() => {
                  setFieldValue("bot", { ...bot, simulation: false });
                  nextStep();
                }}
              >
                Assinar
              </Button>
              <Button
                size="small"
                variant="text"
                className={classes.buttonSimulate}
                style={{
                  color: bot.color,
                }}
                onClick={() => {
                  setFieldValue("bot", { ...bot, simulation: true });
                  nextStep();
                }}
              >
                Simular
              </Button>
            </Flex>
          </div>
        ))}
      </Flex>
    </Container>
  );
};

export default withStyles((theme) => ({
  container: {
    opacity: 0,
    transform: "translateX(-50px)",
    animation: `$translate .5s ease-out forwards`,
  },

  "@keyframes translate": {
    to: {
      opacity: 1,
      transform: "translateX(0)",
    },
  },

  card: {
    width: 240,
    height: 350,
    boxShadow: "2px 2px 30px rgba(0, 0, 0, 0.25)",
    borderRadius: 15,
    margin: "0 16px",
    marginBottom: 18,
  },
  pageTitle: {
    fontWeight: "bold",
    fontSize: 28,
    marginBottom: 8,
  },
  buttonSimulate: {
    background: "transparent",
    marginTop: 6,
    fontSize: 18,
    width: 174,
    height: 42,
  },
  flex: { marginTop: 64 },
  translateY: {
    transform: "translateY(-42px)",
  },
  name: {
    margin: "24px 0",
    fontWeight: "bold",
    fontSize: 24,
  },
  blue: { color: theme.palette.blue },
  yellow: { color: theme.palette.yellow },
  red: { color: theme.palette.red },
}))(Bots);
