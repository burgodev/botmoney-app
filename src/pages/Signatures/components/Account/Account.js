import React from "react";
import { useFormikContext } from "formik";
import PropTypes from "prop-types";
import { withStyles, MenuItem } from "@material-ui/core";

import {
  Flex,
  Typography,
  Card,
  Button,
  TextField,
} from "../../../../_common/components";

PropTypes.propTypes = {
  classes: PropTypes.object,
  values: PropTypes.object,
  nextStep: PropTypes.func,
};

const Account = ({ classes, nextStep }) => {
  const { values, errors, touched, handleChange, setErrors } =
    useFormikContext();

  React.useEffect(
    () =>
      setTimeout(
        setErrors({
          account: {
            id: "",
          },
        }),
        2000
      ),
    [setErrors]
  );

  return (
    <Flex center flexDirection="column" className={classes.flex}>
      <Typography className={classes.pageTitle}>
        {values.bot.simulation
          ? "Simulação com o robô"
          : "Assinatura com o robô"}
      </Typography>
      <Card className={classes.card}>
        <Flex center flexDirection="column">
          <Typography className={classes.name}>{values.bot.name}</Typography>
          {values.bot.icon}
        </Flex>

        <TextField
          className={classes.broker}
          variant="outlined"
          fullWidth
          select
          id="broker"
          type="broker"
          name="account.broker"
          label="Nome da corretora"
          InputLabelProps={{ shrink: true }}
          size="small"
          value={values.account.broker}
          onChange={handleChange}
          error={touched.account?.broker && Boolean(errors.account?.broker)}
          helperText={touched.account?.broker && errors.account?.broker}
          disabled
        >
          <MenuItem value={"Select Markets"}>Select Markets</MenuItem>
        </TextField>

        <TextField
          className={classes.id}
          color={values.bot.color}
          variant="outlined"
          fullWidth
          select
          id="id"
          type="id"
          name="account.id"
          label="ID da conta"
          InputLabelProps={{ shrink: true }}
          size="small"
          value={values.account.id}
          onChange={handleChange}
          error={touched.account?.id && Boolean(errors.account?.id)}
          helperText={touched.account?.id && errors.account?.id}
        >
          <MenuItem value={"496437"}>496437</MenuItem>
          <MenuItem value={"875329"}>875329</MenuItem>
          <MenuItem value={"278313"}>278313</MenuItem>
        </TextField>

        <Button
          className={classes.button}
          style={{
            boxShadow: `1px 1px 20px ${values.bot?.color}`,
            background: values.bot?.color,
          }}
          type="submit"
        >
          Confirmar
        </Button>
      </Card>
    </Flex>
  );
};

export default withStyles((theme) => ({
  flex: {
    opacity: 0,
    transform: "translateX(-50px)",
    animation: `$translate .5s ease-out forwards`,
  },

  "@keyframes translate": {
    to: {
      opacity: 1,
      transform: "translateX(0)",
    },
  },
  card: {
    width: 440,
    height: 380,
    background: "transparent",
  },
  pageTitle: {
    fontWeight: "bold",
    fontSize: 28,
  },
  name: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 8,
    marginTop: 40,
  },
  broker: {
    marginTop: 40,
  },
  id: {
    marginTop: 24,
  },
  button: {
    width: "100%",
    height: 42,
    marginTop: 32,
  },
}))(Account);
